<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs_history', function(Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->string('process_name', 40);
            $table->string('job', 40);
            $table->integer('status')->default(0);
            $table->timestamps();

            if (Schema::hasColumn('users', 'id'))
            {
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('jobs_history');
        Schema::enableForeignKeyConstraints();
    }
}
