<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');

            $table->string('societe')->nullable();
            $table->string('nom');
            $table->string('prenom');
            $table->string('job')->nullable();
            $table->string('tel')->nullable();
            $table->string('fax')->nullable();
            $table->string('line1')->nullable();
            $table->string('line2')->nullable();
            $table->string('ville')->nullable();
            $table->string('etat')->nullable();
            $table->string('cp')->nullable();
            $table->string('pays')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('site_web')->nullable();
            $table->string('email');
            $table->text('complement_info')->nullable();
            $table->string('langue')->nullable();
            $table->integer('id_show')->nullable();
            $table->text('infos_privee')->nullable();
            $table->integer('creer_par');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
