<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MailsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mails_history', function(Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->string('process_name', 40);
            $table->text('mail_body');
            $table->text('mail_content');
            $table->string('mail_from', 150);
            $table->string('mail_subject', 200);
            $table->longText('mailto_id');
            $table->longText('spam_repport');
            $table->string('spam_score', 10);
            $table->string('total_client', 10);
            $table->timestamps();

            if (Schema::hasColumn('users', 'id'))
            {
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('mails_history');
        Schema::enableForeignKeyConstraints();
    }
}
