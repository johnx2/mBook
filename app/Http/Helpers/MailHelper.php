<?php

namespace App\Helpers;

use Carbon\Carbon;
// use DB;
// use Hash;
use Mail;
use App\Client;
use App\Smtp;
use Riverline\SpamAssassin\PostmarkWebservice;

class MailHelper
{
    // $rawEmail = "Subject: Test spam mail (GTUBE)
    // Message-ID: <GTUBE1.1010101@example.net>
    // Date: Wed, 23 Jul 2003 23:30:00 +0200
    // From: Sender <sender@example.net>
    // To: Recipient <recipient@example.net>
    // Precedence: junk
    // MIME-Version: 1.0
    // Content-Type: text/plain; charset=us-ascii
    // Content-Transfer-Encoding: 7bit
    // This is the GTUBE, the
    //     Generic
    //     Test for
    //     Unsolicited
    //     Bulk
    //     Email
    // If your spam filter supports it, the GTUBE provides a test by which you
    // can verify that the filter is installed correctly and is detecting incoming
    // spam. You can send yourself a test mail containing the following string of
    // characters (in upper case and with no white spaces and line breaks):
    // XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X
    // You should send this test mail from an account outside of your network.";

    // $spamAssassin = new PostmarkWebservice($rawEmail, true);
    // echo $spamAssassin->getScore();
    // echo $spamAssassin->getReport();
    // var_dump($spamAssassin->getReportAsArray());

    public static function getCheckFromSpamAssassin($request){

        // On recupere les infos du smtp adresse demandé :
        $smtpInfos = Smtp::where('adresse_smtp', '=', $request->smtp)->first();
        // On envoie le mail fantome et on recupere le callback :
        Mail::send('email.emptyBodyMail', ['content' => $request->message], function ($message) use ($request, $smtpInfos, &$headers) {

            // SMTP dynamique :
            $message->from($smtpInfos['adresse_smtp'], $smtpInfos['label']);
            // Subject :
            $message->subject($request->sujet);
            // MailTo fantome :
            $message->to('checkmails@mineralphilmadagascar.com', 'Check Mail');
            // On callback le $message->getSwiftMessage() :
            $headers = $message->getSwiftMessage();

        });
        // Test du callback dans le PostmarkWebservice() Spamassassin :
        $spamAssassin = new PostmarkWebservice((string)$headers, true);

        $rulesToFr = [
            "BODY_SINGLE_WORD"          => [
                "info" => "Le corps du message est composé d'un seul mot (sans espaces)",
                "complement" => "N-A"
            ],
            "HTML_MESSAGE"              => [
                "info" => "BODY: HTML included in message",
                "complement" => "Les messages HTML sont plus attrayants que le texte brut."
            ],
            "HTML_OBFUSCATE_10_20"      => [
                "info" => "Le message contient 10 à 20% d’obscurcissements HTML",
                "complement" => "Le message comprend du code HTML avec texte obscur, tel que le codage hexadécimal inutile des caractères ASCII. C'est probablement une tentative d'éviter les filtres en mode texte."
            ],
            "LONGWORDS"                 => [
                "info" => "Longue chaîne de longs mots",
                "complement" => "N-A"
            ],
            "MIME_HTML_ONLY"            => [
                "info" => "BODY: Le message contient uniquement des parties MIME text/html",
                "complement" => "Indique que le message ne contient pas la partie alternative en texte brut."
            ],
            "MISSING_HEADERS"           => [
                "info" => "To manquant: HEADERS",
                "complement" => "L'en-tête de courrier ne contient pas de ligne To:"
            ],
            "MISSING_SUBJECT"           => [
                "info" => "Sujet manquant: HEADERS",
                "complement" => "Le message n'a pas de sujet d'en-tête."
            ],
            "NO_RELAYS"                 => [
                "info" => "Informatif: le message n'a pas été relayé via SMTP",
                "complement" => "Cela indique aucun en-tête \"Reçu\" dans le courrier."
            ],
            "TO_NO_BRKTS_HTML_ONLY"     => [
                "info" => "To: manque de crochets et HTML seulement",
                "complement" => "N-A"    
            ],
        ];

        // On recupere le rapport Spam en Array :
        $rules = $spamAssassin->getReportAsArray();
        // Traduction des rules en FR :
        foreach($rules as $pos => $rule){
            foreach($rulesToFr as $key => $value){
                if($key == $rule['name']){
                    $rules[$pos]['info'] = $value['info'];
                    $rules[$pos]['complement'] = $value['complement'];
                }
            }
            $rules[$pos]['complement'] = (isset($rules[$pos]['complement']) == true)? $rules[$pos]['complement'] : 'N-A';
        }
        // Recuperation des infos total :
        $return['totalFrom'] = count($request->arrayId);
        $return['mailContent'] = (string)$headers;
        $return['mailFrom'] = $request->smtp;
        $return['mailToId'] = $request->arrayId;
        $return['mailSubject'] = $request->sujet;
        $return['mailBody'] = $request->message;
        $return['spamScore'] = $spamAssassin->getScore();
        $return['spamReport'] = $rules;
        $return['error'] = 0;

        return $return;

    }

    public static function usersMailsBySmtp($arrayId, $mailSmtp){

        // On recupere les infos du smtp adresse demandé :
        $smtpInfos = Smtp::where('adresse_smtp', '=', $mailSmtp)->first();
        // On recupere les mails des clients :
        $usersMails = Client::find($arrayId);
        $usersMail = $usersMails->pluck('email');
        // On coupe le nbr par le max envoie par heure :
        // $usersMail = $usersMail->chunk(1);
        $usersMail = $usersMail->chunk($smtpInfos['mail_par_heure']);
        // On retourne le tableau decoupé :
        return $usersMail;

    }

    public static function mailRepport($to, $process_name, $totalClient, $totalSuccess, $smtpInfos){

        $dateNow = Carbon::now();
        $dateNow = $dateNow->format("d/m/Y à H:i:s");

        Mail::send('email.repportBodyMail', ['totalClient' => $totalClient, 'totalSuccess' => $totalSuccess, 'process_name' => $process_name], function ($message) use ($smtpInfos, $to, $dateNow) {
                
            $message->from($smtpInfos['adresse_smtp'], $smtpInfos['label']);
        
            $message->subject('Rapport envoi mail du ' . $dateNow);
            $message->to($to);
            $message->bcc(array('john.s.xerri@gmail.com'));

        });

    }

    // static function sendMail($template, $email, Array $options = [], $subject = "Un sujet de test"){
    //     return (isset($template) && isset($email) && !empty($template) && !empty($email)) ?
    //     (env('APP_ENV') == 'production') ?
    //         Mail::send("email.".$template, $options, function($message) use ($email, $subject){
    //             $message->from('no-reply@mineralphilmadagascar.com', 'Mineralphil Madagascar');
    //             $message->cc($email);
    //             $message->subject($subject);
    //         }) : Mail::send("email.".$template, $options, function($message) use ($email, $subject){
    //             $message->from('no-reply@mineralphilmadagascar.com', 'Mineralphil Madagascar');
    //             $message->cc(array('john.s.xerri@gmail.com', 'grazie43@hotmail.com'));
    //             $message->subject($subject);
    //         })
    //     : false;
    // }
}
