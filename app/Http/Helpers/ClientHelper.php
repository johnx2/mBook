<?php

namespace App\Http\Helpers;

class ClientHelper
{

    public static function getCoord($clients, $bool = true){

        $blocs = $clients->chunk(50);
        $modif = [];
        foreach ($blocs as $k => $clients) {
            foreach ($clients as $key => $value) {
                $adresse = '';
                $lat = '';
                $lng = '';
                // On recupere l adresse complete :
                $oldAdress = $value->line1 . ' ' . $value->line2 . ' ' . $value->ville . ' ' . $value->etat . ' ' . $value->cp . ' ' . $value->pays;
                // On filtres le surplus :
                $oldAdress = trim($oldAdress);
                $oldAdress = str_replace(' ', '%20', $oldAdress);
                // On check la nouvelle adresse si exist :
                if (!empty($oldAdress)) {
                    $clientGuzzle = new \GuzzleHttp\Client();
                    $response = $clientGuzzle->request('GET', "https://www.mapquestapi.com/geocoding/v1/address?key=OADhV0g2OOpcCPe9utSxXeEyL7sAbivS&inFormat=kvp&outFormat=json&location=" . $oldAdress, ['verify' => false]);
                    $contents = json_decode((string)$response->getBody());
                    if(isset($contents->results[0])){
                        $content = $contents->results[0]->locations[0];
                        $lat = $content->latLng->lat;
                        $lng = $content->latLng->lng;
                        $adresse = $content->street . ' ' . $content->adminArea6 . ' ' . $content->postalCode . ' ' . $content->adminArea5 . ' ' . $content->adminArea3 . ' ' . $content->adminArea1;
                        
                        // On met a jour les clients si l adresse est trouvée ;) :
                        $value->line1 = (string)$content->street;
                        $value->line2 = (string)$content->adminArea6;
                        $value->cp = (string)$content->postalCode;
                        $value->ville = (string)$content->adminArea5;
                        $value->etat = (string)$content->adminArea3;
                        $value->pays = (string)$content->adminArea1;
                        $value->lat = (string)$lat;
                        $value->lng = (string)$lng;
                    }
                }

                if($bool == true){
                    // On recupere l ancienne adresse :
                    $modif[$value->id]['oldAdress'] = str_replace('%20', ' ', $oldAdress);;
                    // On recupere la nouvelle adresse :
                    $modif[$value->id]['newAdress'] = $adresse;
                    // On show les nouvelles coordonnées :
                    $modif[$value->id]['lat'] = $lat;
                    $modif[$value->id]['lng'] = $lng;
                }

            }
        }

        if($bool == true){
            echo '<p><strong>Vérification des données avant changement</strong></p>';
            echo '<table style="border-color:#000;" class="table">';
            echo '<tr><th>Id client</th><th>Ancienne adresse</th><th>Nouvelle adresse</th><th>Coord Lat</th><th>Coord Lng</th></tr>';
            foreach ($modif as $key => $value) {
                echo '<tr><td>' . $key . '</td><td>' . $value['oldAdress'] . '</td><td>' . $value['newAdress'] . '</td><td>' . $value['lat'] . '</td><td>' . $value['lng'] . '</td></tr>';
            }
            echo '</table>';
        }
        
        // On retourne le tableau decoupé :
        return $clients;

    }

    
}
