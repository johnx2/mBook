<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Date\Date;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $session_token = $request->session()->get('token', null);
        //$this->sessions()->flush();

        //dd($session_token->expires_in);

        if (($session_token === null) || ($session_token->expires_in <= Date::now())) {
            auth()->logout();
            $request->session()->flush();
            return redirect('/login');
        }

        return $next($request);
    }
}
