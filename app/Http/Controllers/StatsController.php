<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\User;
use App\Smtp;
use App\MailsHistory;
use App\JobHistory;

class StatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mailHistory = MailsHistory::All();
        $mailHistory = $mailHistory->sortByDesc('created_at');
        $smtp = Smtp::All();

        $arrayHistoryMail = [];
        foreach ($smtp as $k => $v) {
            foreach ($mailHistory->toArray() as $key => $histo) {
                $arrayHistoryMail[$key]['id'] = $histo['id'];
                $user = User::where('id', $histo['user_id'])->first();
                $arrayHistoryMail[$key]['user_id'] = $user->name;
                $arrayHistoryMail[$key]['process_name'] = $histo['process_name'];
                $arrayHistoryMail[$key]['mail_subject'] = $histo['mail_subject'];
                $arrayHistoryMail[$key]['mail_body'] = $histo['mail_body'];
                $arrayHistoryMail[$key]['total_client'] = $histo['total_client'];

                if($histo['mail_from'] == $v->adresse_smtp){
                    $arrayHistoryMail[$key]['mail_from_label'] = $v->label;
                    $arrayHistoryMail[$key]['mail_from_adresse_smtp'] = $v->adresse_smtp;
                }
                $date = Carbon::parse($histo['created_at']);
                $dateNow = Carbon::now();
                $mois = ['', 'jan', 'fév', 'mar', 'avr', 'mai', 'jui', 'jui', 'aoû', 'sep', 'oct', 'nov', 'déc'];
                $nbrMois = $date->format("n");
                $arrayHistoryMail[$key]['created_at_humain'] = ($date > $dateNow->subWeek()) ? $date->diffForHumans(): $date->format("d") . ' ' . $mois[$nbrMois];
                $arrayHistoryMail[$key]['created_at'] = $histo['created_at'];
            }
        }
        // $arrayHistoryMail = [];

        return view('stats.index', compact('arrayHistoryMail'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($processName)
    {
        $processId = $processName;
        // Details du mail:
        $processName = MailsHistory::where('process_name', $processId)->first();
        if($processName != null){
            $processName->toArray();
            // Details des envoies :
            $processContent = JobHistory::where('process_name', $processId)->get();
            // Recuperation Smtp :
            $smtp = Smtp::where('adresse_smtp', $processName['mail_from'])->first();
            
            $arrayHistoryMail = [];

            $arrayHistoryMail['id'] = $processName['id'];
            $user = User::where('id', $processName['user_id'])->first();
            $arrayHistoryMail['user_name'] = $user->name;
            $arrayHistoryMail['user_mail'] = $user->email;
            $arrayHistoryMail['process_name'] = $processName['process_name'];
            $arrayHistoryMail['mail_subject'] = $processName['mail_subject'];
            $arrayHistoryMail['mail_body'] = $processName['mail_body'];
            $arrayHistoryMail['total_client'] = $processName['total_client'];
            $arrayHistoryMail['mailto_id'] = json_decode($processName['mailto_id']);
            $arrayHistoryMail['spam_repport'] = json_decode($processName['spam_repport']);
            $arrayHistoryMail['spam_score'] = $processName['spam_score'];

            $arrayHistoryMail['mail_from_label'] = $smtp->label;
            $arrayHistoryMail['mail_from_adresse_smtp'] = $smtp->adresse_smtp;
            
            $date = Carbon::parse($processName['created_at']);
            $dateNow = Carbon::now();
            $mois = ['', 'jan', 'fév', 'mar', 'avr', 'mai', 'jui', 'jui', 'aoû', 'sep', 'oct', 'nov', 'déc'];
            $nbrMois = $date->format("n");
            $arrayHistoryMail['created_at_humain'] = ($date > $dateNow->subWeek()) ? $date->diffForHumans(): $date->format("d") . ' ' . $mois[$nbrMois];
            $arrayHistoryMail['created_at'] = $processName['created_at']->format("d/m/Y à H:i:s");

            // dump($arrayHistoryMail);
            
            return view('stats.mailDetail', compact('arrayHistoryMail', 'processContent'));
        } else {
            abort(404);
        }
    }

    public function getStatsMail($processName){

        $processId = $processName;
        // Details des envoies :
        $processContent = JobHistory::where('process_name', $processId)->get();

        return $processContent;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
