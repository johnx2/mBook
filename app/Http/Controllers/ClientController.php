<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User, App\Client, App\Langues, App\Shows;
use ClientHelper;

use Excel;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Validation check email
     * 
     * @param string $email
     * @return bool true or false
     */
    public function checkEmail(Request $request)
    {
        $email = $request['email'];
        $testmail = filter_var($email, FILTER_VALIDATE_EMAIL);
        if($testmail !== false){
            $arrayMail = explode("@", $email);
            if( checkdnsrr(array_pop($arrayMail), "MX") ){
                return "domaine et adresse valide";
            } else {
                return "domaine inexistant, adresse invalide";
            }
        } else {
            return "Adresse e-mail non valide";
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $clients = Client::all()->reverse();
        $shows = Shows::all();
        $langues = Langues::all();

        // $test = ClientHelper::getCoord($clients);
        // dd($test);
        
        foreach($clients as $client){
            if($client->langue !== null){
                $client->langue = json_decode($client->langue);
                $arrayImg = [];
                foreach($client->langue as $langId){
                    foreach($langues as $lang){
                        if((int)$langId == $lang->id){
                            $arrayImg[$lang->id] = $lang;
                        }
                    }
                }
                $client->langue = $arrayImg;
            }
            if($client->id_show !== null){
                $client->id_show = json_decode($client->id_show);
                $arrayShow = [];
                foreach($client->id_show as $showId){
                    foreach($shows as $show){
                        if((int)$showId == $show->id){
                            array_push($arrayShow, $show->name);
                        }
                    }
                }
                $client->id_show = $arrayShow;
            }
        }
        
        return view('clients.listeClients', compact('clients', 'shows', 'langues'));

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajout()
    {
        $langues = Langues::all();
        $shows = Shows::all();

        return view('clients.ajoutClients', compact('langues','shows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Avant tout : On verifie que les donnees principal dont nous avons besoin soit la :
        if($request['societe'] == null || $request['nom'] == null || $request['email'] == null || !isset($request['langue']))
            return redirect()->route('ajout-clients')->with('error', 'Des données sont manquantes !');

        // On recupere les infos de l user connecté :
        $user = auth()->user();

        $client = new Client(); // On declare le Client()
        
        $client->societe = $request['societe'];
        $client->nom = $request['nom'];
        $client->prenom = $request['prenom'];
        $client->job = $request['fonction'];
        $client->tel = $request['tel'];
        $client->fax = $request['fax'];
        $client->line1 = $request['addLine1'];
        $client->line2 = $request['addLine2'];
        $client->ville = $request['ville'];
        $client->etat = $request['etat'];
        $client->cp = $request['cp'];
        $client->pays = $request['pays'];
        $client->lat = $request['lat'];
        $client->lng = $request['lng'];
        $client->site_web = $request['website'];
        $client->email = $request['email'];
        $client->complement_info = $request['complementInfo'];
        $client->infos_privee = $request['private'];

        // On encode les check langue et show en json pour avoir un string ;) :
        $client->langue = json_encode($request['langue']);
        $client->id_show = (isset($request['showB'])) ? json_encode($request['showB']) : null;
        
        // On recupere l id de l user en cours ;) :
        $client->creer_par = $user->id;

        // Et on save :
        $client->save();

        // Si on valide et qu on ajoute un user :
        if( isset($request['formAddSubmitOne']) ){
            
            return redirect()->route('ajout-clients')->with('status', 'Client bien enregistré !');

        }

        // Si on valide et que l on quitte :
        if( isset($request['formAddSubmitTwo']) ){

            return redirect()->route('home')->with('status', 'Client bien enregistré !');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);
        $langues = Langues::all();
        $shows = Shows::all();

        return view('clients.infosClients', compact('client', 'langues', 'shows'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Si on veux juste quitter la page :
        if( isset($request['leave']) ){
            
            return redirect()->route('liste-clients');

        } 

        if( isset($request['update']) ){

            // Avant tout : On verifie que les donnees principal dont nous avons besoin soit la :
            if($request['societe'] == null || $request['nom'] == null || $request['email'] == null || !isset($request['langue']))
                return redirect()->route('ajout-clients.show', ['id' => $id])->with('error', 'Des données sont manquantes !');

            // On recupere les infos de l user connecté :
            $user = auth()->user();

            $client = CLient::find($id);
            
            $client->societe = $request['societe'];
            $client->nom = $request['nom'];
            $client->prenom = $request['prenom'];
            $client->job = $request['fonction'];
            $client->tel = $request['tel'];
            $client->fax = $request['fax'];
            $client->line1 = $request['addLine1'];
            $client->line2 = $request['addLine2'];
            $client->ville = $request['ville'];
            $client->etat = $request['etat'];
            $client->cp = $request['cp'];
            $client->pays = $request['pays'];
            $client->lat = $request['lat'];
            $client->lng = $request['lng'];
            $client->site_web = $request['website'];
            $client->email = $request['email'];
            $client->complement_info = $request['complementInfo'];
            $client->infos_privee = $request['private'];

            // On encode les check langue et show en json pour avoir un string ;) :
            $client->langue = json_encode($request['langue']);
            $client->id_show = (isset($request['showB'])) ? json_encode($request['showB']) : null;
            
            // On recupere l id de l user en cours ;) :
            $client->creer_par = $user->id;
            
            // Et on save :
            $client->save();

            return redirect()->route('ajout-clients.show', ['id' => $id])->with('status', 'Client bien enregistré !');

        }

        if( isset($request['delete']) ){

            $client = Client::find($id);

            $client->delete();

            return redirect()->route('liste-clients')->with('status', 'Client bien supprimé !');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function downoloadListe(){

        $clients = Client::all();
        $shows = Shows::all();
        $langues = Langues::all();
        $employees = User::all();
        
        foreach($clients as $client){

            // Affichage Langues :
            if($client->langue !== null){
                $client->langue = json_decode($client->langue);
                $arrayImg = "";
                foreach($client->langue as $langId){
                    foreach($langues as $lang){
                        if((int)$langId == $lang->id){
                            if($arrayImg == ""){
                                $arrayImg .= $lang->langue;
                            } else {
                                $arrayImg .= ' - ' . $lang->langue;
                            }
                        }
                    }
                }
                $client->langue = $arrayImg;
            }

            // Affichage Shows :
            if($client->id_show !== null){
                $client->id_show = json_decode($client->id_show);
                $arrayShow = "";
                foreach($client->id_show as $showId){
                    foreach($shows as $show){
                        if((int)$showId == $show->id){
                            if($arrayShow == ""){
                                $arrayShow .= $show->name;
                            } else {
                                $arrayShow .= ' - ' . $show->name;
                            }
                        }
                    }
                }
                $client->id_show = $arrayShow;
            }

            // Affichage creer_par :
            if($client->creer_par !== null){
                foreach($employees as $employee){
                    if($employee->id == $client->creer_par){
                        $client->creer_par = $employee->name;
                    }
                }
            }
        }

        $data = $clients->toArray();

        \Excel::create('client_liste_mbook', function($excel) use($data) {

            // Set the title
            $excel->setTitle('Our new awesome title');
        
            // Chain the setters
            $excel->setCreator('mBook')
                  ->setCompany('Mineralphil Madagascar');
        
            // Call them separately
            $excel->setDescription('Liste clients');

            // Our first sheet
            $excel->sheet('Liste Clients', function($sheet) use($data) {

                $sheet->fromArray($data);

            });
        
        })->download('csv');

    }

}
