<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User, App\Client, App\Langues, App\Shows, App\Smtp, App\MailsHistory;
use Mail;

use MailHelper;

use Excel;
use Session;
use Carbon\Carbon;
use App\Jobs\EmailsSender;

class MailController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        // On rafraichie la page si c est un retour arriere depuis stats/mail-detail/xxxx pour eviter le double envoie du formulaire :
        // $precedent = session('_previous')['url'] == url('/') ? '/' : preg_replace('@' . url('/') . '/@', '', session('_previous')['url']);
        // if(preg_match('#^stats/mail-detail/#', $precedent)){
        //     return redirect('/gestion-mails');
        // }

        $clientsMail = [];
        $totalClientMail = 0;

        // Si il y a des id Clients :
        if($request->clientId != null){

            $clients = Client::All();
            foreach($clients as $client){
                foreach($request->clientId as $cId){
                    if($client->id == $cId){
                        // On recrée le tableau de users :
                        array_push($clientsMail, $client);
                    }
                }
            }

            $totalClientMail = count($clientsMail);

        }

        $smtp = Smtp::all();
        $smtpClientMail = [];
        foreach ($smtp as $key => $value) {
            $smtpClientMail[$key]['nom_smtp'] = $value->nom_smtp;
            $smtpClientMail[$key]['label'] = $value->label;
            $smtpClientMail[$key]['total_heure_max'] = (int)ceil($totalClientMail / $value->mail_par_heure);
            $smtpClientMail[$key]['adresse_smtp'] = $value->adresse_smtp;
        }

        return view('gestion-mails.index', compact('clientsMail', 'totalClientMail', 'smtpClientMail'));
    }

    public function checkMail(Request $request){

        if($request->sujet !== null && $request->message !== null){

            $return = MailHelper::getCheckFromSpamAssassin($request);
            return $return;

        } else {

            $return['error'] = 42;
            $return['totalFrom'] = count($request->arrayId);
            return $return;

        }
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $content = $request->content;
        // Récupération des users :
        $usersMails = MailHelper::usersMailsBySmtp($content['mailToId'], $content['mailFrom']);
        // Récupération du FROM :
        $from = $content['mailFrom'];
        // Récupération du subject :
        $subject = $content['mailSubject'];
        // Récupération du body :
        $body = $content['mailBody'];
        // User id en cours :
        $user = auth()->user();
        // Timestamp now();
        $time = Carbon::now()->timestamp;
        $process_name = "$time".'_'.$content['totalFrom'];

        $smtpInfos = Smtp::where('adresse_smtp', '=', $from)->first();

        // Historisation de cette envoie de mail :
        $MailHistory = new MailsHistory;
        $MailHistory->user_id = $user->id;
        $MailHistory->process_name = $process_name;
        $MailHistory->mail_body = $body;
        $MailHistory->mail_content = $content['mailContent'];
        $MailHistory->mail_from = $from;
        $MailHistory->mail_subject = $subject;
        $MailHistory->mailto_id = json_encode($content['mailToId']);
        $MailHistory->spam_repport = json_encode($content['spamReport']);
        $MailHistory->spam_score = $content['spamScore'];
        $MailHistory->total_client = (string)$content['totalFrom'];

        $MailHistory->save();

        // delay + 1n:
        $delays = 10;
        // Boucle sur autant de bloc de mails par heure :
        foreach ($usersMails->toArray() as $key => $value) {
            // Lancement du Job EmailsSender() :
            $envoieFunctionMail = (new EmailsSender($user, $value, $from, $subject, $body, $process_name))->delay(Carbon::now()->addSeconds($delays));
            dispatch($envoieFunctionMail);
            $delays += 15;
        }
        
        // Session::flash('status', 'Mail(s) en cours d\'envoi !');
        return $process_name;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
