<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Mail;
use App\User;
use App\Smtp;
use App\JobHistory;

use MailHelper;

class EmailsSender implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    protected $user;

    /**
     * @var array
     */
    private $usersMail;

    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $body;

    /**
     * @var string
     */
    private $process_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, array $usersMail, string $from, string $subject, string $body, string $process_name)
    {
        $this->user = $user;
        $this->usersMail = $usersMail;
        $this->from = $from;
        $this->subject = $subject;
        $this->body = $body;
        $this->process_name = $process_name;

        foreach ($this->usersMail as $key => $mail) {

            // Set up our new history record.
            $this->history = new JobHistory;
            $this->history->user_id = $this->user->id;
            $this->history->process_name = $this->process_name;
            $this->history->job = $mail;
            $this->history->status = 0;

            $this->history->save();

        }

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sujet = $this->subject;
        $usersMail = $this->usersMail;
        $process_name = $this->process_name;
        $to = $this->user->email;
        
        $smtpInfos = Smtp::where('adresse_smtp', '=', $this->from)->first();

        $totalClient = count($usersMail);
        $totalSuccess = 0;
        foreach ($usersMail as $key => $mail) {
            // Boucle sur chaque mail en TO :
            Mail::send('email.emptyBodyMail', ['content' => $this->body], function ($message) use ($smtpInfos, $sujet, $mail) {
                
                $message->from($smtpInfos['adresse_smtp'], $smtpInfos['label']);
            
                $message->subject($sujet);
                $message->to($mail);

            });

            // Once the job has finished, set history status to 1.
            $thisJobs = JobHistory::where('process_name', $process_name)->where('job', $mail)->first();
            $thisJobs->status = 1;
            $thisJobs->save();
            
            //echo $mail . ' envoyé.' . "\n";
            $totalSuccess++;
        }

        MailHelper::mailRepport($to, $process_name, $totalClient, $totalSuccess, $smtpInfos, $sujet);
    }
}
