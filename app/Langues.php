<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $langue
 * @property string $img
 */
class Langues extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['langue', 'img'];

}
