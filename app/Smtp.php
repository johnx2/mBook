<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nom_smtp
 * @property string $adresse_smtp
 * @property string $label
 * @property int $mail_par_heure
 * @property int $id_user
 */
class Smtp extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'smtp';

    /**
     * @var array
     */
    protected $fillable = ['nom_smtp', 'adresse_smtp', 'label', 'mail_par_heure', 'id_user'];

}
