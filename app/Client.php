<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients';
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['societe', 'nom', 'prenom', 'job', 'tel', 'fax', 'line1', 'line2', 'ville', 'etat', 'cp', 'pays', 'lat', 'lng', 'site_web', 'email', 'complement_info', 'langue', 'id_show', 'infos_privee', 'creer_par'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

}
