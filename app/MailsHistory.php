<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property int $user_id
 * @property string $process_name
 * @property string $mail_body
 * @property string $mail_content
 * @property string $mail_from
 * @property string $mail_subject
 * @property string $mailto_id
 * @property string $spam_repport
 * @property string $spam_score
 * @property string $total_client
 * @property string $created_at
 * @property string $updated_at
 */
class MailsHistory extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'mails_history';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'process_name', 'mail_body', 'mail_content', 'mail_from', 'mail_subject', 'mailto_id', 'spam_repport', 'spam_score', 'total_client', 'created_at', 'updated_at'];

}
