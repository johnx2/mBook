@extends('layouts.app')

@section('content')
    <div class="container infosClientsPage">
        <div class="row m-t-10">
            <!-- Message error-success -->
            @if (session('status'))
                <div class="col-md-12 alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="col-md-12 alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
        </div>
        <div class="row">            

            <div class="col-sm-12 col-md-8 col-md-offset-2">

                <div class="row">
                
                    {{ Form::open(['name' => 'ajout-clients.update', 'method' => 'post', 'id' => $client->id]) }}

                        <ul class="collapsible" data-collapsible="accordion">
                            <li class="active">
                                <div class="collapsible-header collapseOne"><i class="fa fa-address-card-o" aria-hidden="true"></i> Coordonnees</div>
                                <div class="collapsible-body">

                                    <div class="row">

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-building-o prefix"></i>
                                            <input id="societe" name="societe" type="text" class="validate" @if($client->societe != null)value="{{ $client->societe }}" @endif required />
                                            <label for="societe">Société <sup>*</sup></label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-briefcase prefix" aria-hidden="true"></i>
                                            <input id="fonction" name="fonction" type="text" class="validate" @if($client->job != null)value="{{ $client->job }}" @endif />
                                            <label for="fonction">Fonction</label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-user-circle prefix" aria-hidden="true"></i>
                                            <input id="nom" name="nom" type="text" class="validate" @if($client->nom != null)value="{{ $client->nom }}" @endif required />
                                            <label for="nom">Nom <sup>*</sup></label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-user-circle-o prefix" aria-hidden="true"></i>
                                            <input id="prenom" name="prenom" type="text" class="validate" @if($client->prenom != null)value="{{ $client->prenom }}" @endif />
                                            <label for="prenom">Prénom</label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-phone prefix" aria-hidden="true"></i>
                                            <input id="tel" name="tel" type="tel" class="validate" @if($client->tel != null)value="{{ $client->tel }}" @endif />
                                            <label for="tel">Tél.</label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-fax prefix" aria-hidden="true"></i>
                                            <input id="fax" name="fax" type="tel" class="validate" @if($client->fax != null)value="{{ $client->fax }}" @endif />
                                            <label for="fax">Fax</label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-link prefix" aria-hidden="true"></i>
                                            <input id="website" name="website" type="text" class="validate" @if($client->site_web != null)value="{{ $client->site_web }}" @endif />
                                            <label for="website">Website</label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-envelope-o prefix" aria-hidden="true"></i>
                                            <input id="email" name="email" type="email" class="validate" @if($client->email != null)value="{{ $client->email }}" @endif required />
                                            <label for="email">E-mail <sup>*</sup></label>
                                        </div>

                                        <div class="col s12 checkboxLang">
                                            <div class="card">
                                                <div class="header requiredLang">
                                                    <i class="fa fa-globe" aria-hidden="true"></i> Choisir une langue <sup>*</sup>
                                                </div>
                                                <div class="body">
                                                    <div class="row">
                                                        <div class="col s12 m6">
                                                            <select id="lang" multiple required name="langue[]">
                                                                <option value="" disabled @if($client->langue == null) selected @endif >Choisir une langue</option>
                                                                @php $totalLangues = ($client->langue != null) ? json_decode($client->langue) : [] ; @endphp
                                                                @foreach($langues as $langue)
                                                                    <option class="valueLangContent" value="{{ $langue->id }}" @if(in_array($langue->id, $totalLangues)) selected @endif >{{ $langue->langue }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    
                                </div>
                            </li>
                            <li>
                                <div class="collapsible-header collapseTwo"><i class="fa fa-street-view" aria-hidden="true"></i> Adresse</div>
                                <div class="collapsible-body">

                                    <div class="row">

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-road prefix"></i>
                                            <input id="addLine1" name="addLine1" type="text" class="validate" @if($client->line1 != null)value="{{ $client->line1 }}" @endif />
                                            <label for="addLine1">N° + Rue</label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-road prefix"></i>
                                            <input id="addLine2" name="addLine2" type="text" class="validate" @if($client->line2 != null)value="{{ $client->line2 }}" @endif />
                                            <label for="addLine2">Complément adresse</label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-map-signs prefix"></i>
                                            <input id="cp" name="cp" type="text" class="validate" @if($client->cp != null)value="{{ $client->cp }}" @endif />
                                            <label for="cp">Code postal</label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-home prefix"></i>
                                            <input id="ville" name="ville" type="text" class="validate" @if($client->ville != null)value="{{ $client->ville }}" @endif />
                                            <label for="ville">Ville</label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-university prefix"></i>
                                            <input id="etat" name="etat" type="text" class="validate" @if($client->etat != null)value="{{ $client->etat }}" @endif />
                                            <label for="etat">Etat</label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-globe prefix"></i>
                                            <input id="pays" name="pays" type="text" class="validate" @if($client->pays != null)value="{{ $client->pays }}" @endif />
                                            <label for="pays">Pays</label>
                                        </div>

                                        <div class="input-field col s6">
                                            <input id="lat" name="lat" type="text" class="validate" @if($client->lat != null)value="{{ $client->lat }}" @endif />
                                            <label for="lat">Lat</label>
                                        </div>

                                        <div class="input-field col s6">
                                            <input id="lng" name="lng" type="text" class="validate" @if($client->lng != null)value="{{ $client->lng }}" @endif />
                                            <label for="lng">Lng</label>
                                        </div>

                                        <button class="btn btn-warning waves-effect waves-light col s6 offset-s3" type="button" name="formFindAll">
                                            Trouve le reste
                                            <i class="fa fa-refresh right" aria-hidden="true"></i>
                                        </button>

                                        <button type="button" name="formEraseLocality" class="btn btn-primary col m1 offset-m1 s6 offset-s3">
                                            <i class="fa fa-eraser" aria-hidden="true"></i>
                                        </button>

                                    </div>
                                    
                                </div>
                            </li>
                            <li>
                                <div class="collapsible-header"><i class="material-icons">favorite_border</i> Préférences client</div>
                                <div class="collapsible-body">

                                    <div class="row">

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-info prefix"></i>
                                            <textarea id="complementInfo" name="complementInfo" class="materialize-textarea">@if($client->complement_info != null){{ $client->complement_info }} @endif</textarea>
                                            <label for="complementInfo">Complement d’information</label>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <i class="fa fa-diamond prefix"></i>
                                            <textarea id="private" name="private" class="materialize-textarea">@if($client->infos_privee != null){{ $client->infos_privee }} @endif</textarea>
                                            <label for="private">Préférence pierre et prix</label>
                                        </div>

                                        <div class="col s12 checkboxShow">
                                            <div class="card">
                                                <div class="header">
                                                    <i class="fa fa-globe" aria-hidden="true"></i> Préférence show
                                                </div>
                                                <div class="body">
                                                    <div class="row">
                                                        <div class="col s12 m4">

                                                            <select id="showB" multiple required name="showB[]">
                                                                <option value="" disabled @if($client->id_show == null) selected @endif >Préférence show</option>
                                                                @php $totalShow = ($client->id_show != null) ? json_decode($client->id_show) : []; @endphp
                                                                @foreach($shows as $showB)
                                                                    <option class="valueShowContent" value="{{ $showB->id }}" @if(in_array($showB->id, $totalShow))selected @endif >{{ $showB->name }}</option>            
                                                                @endforeach
                                                            </select>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </li>
                        </ul>

                        <button class="btn btn-warning waves-effect waves-light formAddEditOne" type="submit" name="update">
                            Modifier
                            <i class="material-icons right">cached</i>
                        </button>

                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target="#myModal">
                            Supprimer
                            <i class="material-icons right">send</i>
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Etes-vous sure ?</h4>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-warning waves-effect waves-light" data-dismiss="modal">Annuler</button>
                                        <button type="submit" class="btn btn-warning waves-effect waves-light formAddEditTwo" name="delete">
                                            Supprimer
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-warning waves-effect waves-light formAddEditThree" type="submit" name="leave">
                            Quitter
                            <i class="material-icons right">keyboard_return</i>
                        </button>

                    {{ Form::close() }}

                </div>

            </div>

        </div>
    </div>
@endsection

@section('js')

    <!-- Ajout de la scrollbar-y en auto -->
    <script>
        $('body').css({ 'overflow-y': 'scroll' });
    </script>

    <script>
        $(document).ready(function(){

            $('.collapsible').collapsible();
            $('select').formSelect();

            /**
             * Gestion du formulaire ADD CLIENT - Vérification du mail
             */

                $('[name="email"]').on('keyup', function(){
                    
                    // On recuere le mail fraichement inseré
                    var emailContent = $(this).val();
                    
                    // On lance l'ajax passion Eucaliptus
                    $.ajax({

                        type: 'POST',
                        url: '{{ url("ajout-clients/checkemail") }}',
                        data: { _token: "{{ csrf_token() }}", email: emailContent },
                        // dataType: 'JSON',
                        error: function(result, status, error) { 
                            console.log("Réponse jQuery : " + result);
                            console.log("Statut de la requète : " + status);
                            console.log("Type d’erreur : " + error); 
                        },

                        // Si mon bon ajax me ramene de joli chose
                        success: function(data){
                            // console.log(data);
                            if(data == 'Adresse e-mail non valide' || data == 'domaine inexistant, adresse invalide'){
                                $('[name="email"]').removeClass('valid').addClass('invalid');
                                $('.collapsible .collapseOne').html('<i class="fa fa-address-card-o" aria-hidden="true"></i> Coordonnees <i class="fa fa-info-circle" aria-hidden="true" style="color:#f00;"></i> <span style="color:#f00;">' + data + '</span>');
                            } else {
                                $('[name="email"]').removeClass('invalid').addClass('valid');
                                $('.collapsible .collapseOne').html('<i class="fa fa-address-card-o" aria-hidden="true"></i> Coordonnees');
                            }
                        }

                    });

                });

            /**
             * Gestion du formulaire ADD CLIENT - Recuperation coordonnee depuis adresse
             */
            
                // Si on genere l adresse : 
                $('[name="formFindAll"]').on('click', function(z){
                    z.preventDefault();

                    var formAddLine1 	= $('#addLine1').val();
                        formAddLine2 	= $('#addLine2').val();
                        formAddTown	 	= $('#ville').val();
                        formAddState 	= $('#etat').val();
                        formAddCP    	= $('#cp').val();
                        formAddCountry 	= $('#pays').val();

                    var formAddAddress = formAddLine1 + ' ' + formAddLine2 + ' ' + formAddTown + ' ' + formAddState + ' ' + formAddCP + ' ' + formAddCountry;

                    var	formAddAddressBlock = $('.collapsible .collapseTwo');
                    formAddAddressBlock.html('<i class="fa fa-street-view" aria-hidden="true"></i> Adresse');

                    // console.log(formAddAddressBlock);
                    // console.log(formAddLine1 + ' ' + formAddLine2 + ' ' + formAddTown + ' ' + formAddState + ' ' + formAddCP + ' ' + formAddCountry);

                    // Si au moins un des champs n est pas vide
                    if (formAddLine1 != '' || formAddLine2 != '' || formAddTown != '' || formAddState != '' || formAddCP != '' || formAddCountry != '') {

                        // On lance l'ajax passion lavande
                        $.ajax({

                            type: 'GET',
                            // LIMIT 15K request/month : https://developer.mapquest.com/user/me/apps :
                            url: 'https://www.mapquestapi.com/geocoding/v1/address?key=OADhV0g2OOpcCPe9utSxXeEyL7sAbivS&inFormat=kvp&outFormat=json&location=' + formAddAddress,
                            dataType: 'json',

                            // Si mon bon ajax me ramene de joli chose
                            success: function(data){
                                
                                // Un petit check de ce que je recupere ^_^ :
                                // console.log(data.results[0]);

                                // Si data['results']['0'] contient du croustillant
                                if (data.results[0] != undefined) {

                                    var totalResultats = Object.keys(data.results[0].locations).length;
                                    if (totalResultats == 1){

                                        var adresseContenu 	= data.results[0].locations[0];
                                            coord 			= data.results[0].locations[0].latLng;

                                        $.each( adresseContenu, function( key, value ) {

                                            // Recuperation dynamique du num + rue 
                                            if ( key == 'street' ) {
                                                var clientsLine1 = value;
                                                $('input[id="addLine1"]').val(clientsLine1).focus().css({ 'color': 'white' });
                                            }

                                            // Recuperation complement adresse
                                            if ( key == 'adminArea6') {
                                                var clientsLine2 = value;
                                                $('input[id="addLine2"]').val(clientsLine2).focus().css({ 'color': 'white' });
                                            }

                                            // Recuperation dynamique de la ville
                                            if ( key == 'adminArea5' ) {
                                                var clientsTown = value;
                                                $('input[id="ville"]').val(clientsTown).focus().css({ 'color': 'white' });
                                            }

                                            // Recuperation dynamique de l etat
                                            if ( key == 'adminArea3' ) {
                                                var clientsState = value;
                                                $('input[id="etat"]').val(clientsState).focus().css({ 'color': 'white' });
                                            }

                                            // Recuperation dynamique du code postal
                                            if ( key == 'postalCode' ) {
                                                var clientsCP = value;
                                                $('input[id="cp"]').val(clientsCP).focus().css({ 'color': 'white' });
                                            }

                                            // Recuperation dynamique du pays
                                            if ( key == 'adminArea1' ) {
                                                var clientsCountry = value;
                                                $('input[id="pays"]').val(clientsCountry).focus().css({ 'color': 'white' });
                                            }

                                        });

                                        // On vrifie qu on a bien lat et lng
                                        if ( coord.lat != '' && coord.lng != '' ) {

                                            // Recuperation dynamique de la latitude
                                            $('input[id="lat"]').val(coord.lat).focus().css({ 'color': 'white' });
                                            // Recuperation dynamique de la longitude
                                            $('input[id="lng"]').val(coord.lng).focus().css({ 'color': 'white' });

                                            formAddAddressBlock.html('<i class="fa fa-street-view" aria-hidden="true"></i> Adresse <i class="fa fa-thumbs-o-up" aria-hidden="true" style="color:#0f0;"></i> <span style="color:#0f0;">Adresse trouvée !</span>');

                                        // Sinon on a pas pu recuperer la gps-isation #triste
                                        } else {

                                            formAddAddressBlock.html('<i class="fa fa-street-view" aria-hidden="true"></i> Adresse <i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:#f00"></i> <span style="color:red;">Adresse non trouvée !</span>');

                                        }
                                    
                                    // Si top de résultats
                                    } else {

                                        formAddAddressBlock.html('<i class="fa fa-street-view" aria-hidden="true"></i> Adresse <i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:#f00"></i><span style="color:red;"> (' + totalResultats + ' résulats) merci d\'ajouter une info !</span>');

                                    }

                                // // Sinon data['results']['0'] est vide donc message d erreur
                                } else {

                                    formAddAddressBlock.html('<i class="fa fa-street-view" aria-hidden="true"></i> Adresse <i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:#f00"></i> <span style="color:red;">Adresse non trouvée !</span>');

                                }

                            },

                            // error de bonne et due forme
                            error: function(){
                                console.log('Bad request .. Try again Dude!');
                                formAddAddressBlock.html('<i class="fa fa-street-view" aria-hidden="true"></i> Adresse <i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:#f00;"></i> <p style="color:#f00;">Une erreur $.ajax s\'est produite ! Contacter l\'admin svp !</p>');
                            }

                        }); // End $.ajax();

                    // Sinon c est que les champs sont vides
                    } else {

                        formAddAddressBlock.html('<i class="fa fa-street-view" aria-hidden="true"></i> Adresse <i class="fa fa-info-circle" aria-hidden="true" style="color:#f00;"></i> <span style="color:#f00;">Champs vide !</span>');

                    }

                }); // End click()

                // Si on efface ce qui n est pas correct :
                $('[name="formEraseLocality"]').on('click', function(){
                    $('#addLine1').val('').next('label').removeClass('active');
                    $('#addLine2').val('').next('label').removeClass('active');
                    $('#ville').val('').next('label').removeClass('active');
                    $('#etat').val('').next('label').removeClass('active');
                    $('#cp').val('').next('label').removeClass('active');
                    $('#pays').val('').next('label').removeClass('active');
                    $('#lat').val('').next('label').removeClass('active');
                    $('#lng').val('').next('label').removeClass('active');
                });

            /**
             * Gestionnaire d'erreurs :
             */

                $('button[type="submit"]').on('click', function(e){

                    // Gestion des erreurs liées à la langue client :
                    var	formAddAddressBlock = $('.requiredLang');
                    if($('.valueLangContent:selected').length == 0){
                        e.preventDefault();
                        formAddAddressBlock.html('<i class="fa fa-globe" aria-hidden="true"></i> Choisir une langue <sup>*</sup> <i class="fa fa-info-circle" aria-hidden="true" style="color:#f00;"></i> <span style="color:#f00;">Champs requis !</span>');
                    } else {
                        formAddAddressBlock.html('<i class="fa fa-globe" aria-hidden="true"></i> Choisir une langue <sup>*</sup>');
                    }

                    // Gestion des erreurs du bloc 1 :
                    var	formBlockOne = $('.collapsible .collapseOne');
                    if($('.valueLangContent:selected').length == 0 || $('#societe').val() == '' || $('#nom').val() == '' || $('#email').val() == ''){
                        formBlockOne.html('<i class="fa fa-address-card-o" aria-hidden="true"></i> Coordonnees <i class="fa fa-info-circle" aria-hidden="true" style="color:#f00;"></i> <span style="color:#f00;">Champs manquant !</span>');
                    } else {
                        formBlockOne.html('<i class="fa fa-address-card-o" aria-hidden="true"></i> Coordonnees');
                    }

                });

        });
    </script>
@endsection