@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
@endsection

@section('content')
<div class="wrapper">
    Chargement ..
  <div class="loader">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
  </div>
</div>
<div class="container listeClientsPage">

    <!-- AFFICHAGE DES MESSAGES D ERREURS -->
    <div class="row m-t-10">
        <!-- Message error-success -->
        @if (session('status'))
            <div class="col-md-12 alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @if (session('error'))
            <div class="col-md-12 alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
    </div>

    <!-- BUTTON GROUP SEARCH -->
    {{-- <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2 hide-on-med-and-down">

            <div class="btn-group" role="group">
                @foreach($shows as $show)
                    <button class="searchShowLang" data-name="{{ $show->name }}">{{ $show->name }}</button>
                @endforeach
            </div>

        </div>
        <div class="col-sm-4 col-md-8 col-md-offset-2">
            
            <div class="btn-group hide-on-med-and-down" role="group">
                @foreach($langues as $langue)
                    <button class="searchShowLang" data-name="{{ $langue->langue }}">{{ $langue->langue }}</button>
                @endforeach
            </div>

            <div class="btn-group hide-on-med-and-down" role="group">
                <button class="searchShowLang" data-name="All">Tout</button>
            </div>

            <div class="btn-group" role="group">
                
            </div>

        </div>
    </div> --}}

    <!-- ARRAY -->
    <div class="row">            

        <div class="col-sm-12 col-md-10 col-md-offset-1">
            
            {{ Form::open(['route' => 'gestion-mails.indexPost', 'method' => 'post']) }}

                <table id="example" class="table display select table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>
                                <input name="select_all" value="1" id="example-select-all" type="checkbox" />
                            </th>
                            <th>Nom Prénom</th>
                            <th>Société</th>
                            <th class="hidden-xs">Pays</th>
                            <th class="hidden-xs">Show</th>
                            <th class="hidden-xs">Langue(s)</th>
                            <th>-</th>
                            <th style="display:none!important;"></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($clients as $client)
                            <!-- {{ dump($client) }} -->
                            <tr class="tr-click">
                                <td class="td-input">
                                    {{ $client->id }}
                                </td>
                                <td>{{ $client->nom . ' ' . $client->prenom }}</td>
                                <td>{{ $client->societe }}</td>
                                <td class="hidden-xs">
                                    @if($client->pays != null)
                                        {{ $client->pays }}
                                    @else 
                                        <span>-</span>
                                    @endif
                                </td>
                                <td class="hidden-xs">
                                    @if($client->id_show != null)
                                        @foreach($client->id_show as $show)
                                            <span data-info="{{ $show }}">{{ $show }}</span>
                                        @endforeach
                                    @else 
                                        <span>-</span>
                                    @endif
                                </td>
                                <td class="hidden-xs">
                                    @if($client->langue != null)
                                        @foreach($client->langue as $langue)
                                            @foreach(json_decode($langue) as $key => $lang)
                                                @if($key == "img")
                                                    <img data-info="{{ $langue['langue'] }}" src="{{ asset('img/' . $langue['img']) }}" alt="{{ $langue['langue'] }}" title="{{ $langue['langue'] }}" width="32px" />
                                                    <span style="display:none!important;">{{ $langue['langue'] }}</span>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @else 
                                        <span>-</span>
                                    @endif
                                </td>
                                <td class="fa-edit-td"><a href="{{ url('info-client', ['id' => $client->id]) }}"><i class="fa fa-edit" aria-hidden="true"></i></a></td>
                                <td style="display:none!important;"></td>

                            </tr>
                        @endforeach
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Nom Prénom</th>
                            <th>Société</th>
                            <th class="hidden-xs">Pays</th>
                            <th class="hidden-xs">Show</th>
                            <th class="hidden-xs">Langue(s)</th>
                            <th>-</th>
                            <th style="display:none!important;"></th>
                        </tr>
                    </tfoot>

                </table>

                <button type="button" class="downloadList btn btn-warning" title="Exporter la liste client">    
                    <a href="{{ url('downoloadListe') }}"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                </button>
            
                @if(sizeof($clients) == 0)
                    <button type="button" id="frm-example" name="frm-example" class="btn disabled"><i class="fa fa-envelope-o" aria-hidden="true"></i> Envoyer un mail</button>
                @else
                    <button type="submit" id="frm-example" name="frm-example" class="btn btn-warning"><i class="fa fa-envelope-o" aria-hidden="true"></i> Envoyer un mail</button>
                @endif
            
            {{ Form::close() }}

        </div>

    </div>
    
</div>
@endsection

@section('js')

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
    

    <script>

        // Ajout d'une scrollbar native :
        $('html').css({ 'overflow-y': 'scroll' });

        $(document).ready(function (){
            // Si la fenetre est plus petite que 767px on n affiche que 3 buttons pagination :
            var largeurWindow = $(window).width();
            if (largeurWindow <= 767){
                $.fn.DataTable.ext.pager.numbers_length = 3;
            }
            var table = $('#example').DataTable({
                'columnDefs': [
                    {
                        'targets': 0,
                        'render': function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<label><input type="checkbox" class="dt-checkboxes"><span></span></label>';
                        }

                        return data;
                        },
                        'checkboxes': {
                            'selectRow': true,
                            'selectAllRender': '<label><input type="checkbox" class="dt-checkboxes"><span></span></label>'
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                "scrollCollapse": true,
                "paging":         true,
                "ordering": false,
                "info":     true,
                'order': [[1, 'desc']],
                "language": {
                    processing:     "Traitement en cours...",
                    search:         "Rechercher&nbsp;:",
                    lengthMenu:    "Afficher _MENU_ client(s)",
                    info:           "Affichage de client _START_ &agrave; _END_ sur _TOTAL_ client(s)",
                    infoEmpty:      "Affichage de client 0 &agrave; 0 sur 0 client(s)",
                    infoFiltered:   "(filtr&eacute; de _MAX_ client(s) au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun client &agrave; afficher",
                    emptyTable:     "Vous ne pos&eacute;der auncun client",
                    paginate: {
                            first:      "Premier",
                            previous:   "Pr&eacute;c&eacute;dent",
                            next:       "Suivant",
                            last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                    buttons: {
                        selectAll: "Select all items",
                        selectNone: "Select none"
                    }
                }
            });

            // Gestion du checkbox par tr (en clickant sur une ligne) :
            $('td').on('click', function() {
                if(!$(this).hasClass('fa-edit-td')) {
                    if(!$(this).hasClass('td-input')) {
                        $(this).parent('tr').find('input.dt-checkboxes').click();
                    }
                    // $(this).parent().find('input[type="checkbox"]').prop( "checked", function( i, val ) { return !val; } );
                }
            });

            // Handle form submission event
            $('#frm-example').on('click', function(e){
                // $(this).attr('disabled', 'disabled');
                var form = this;

                var rows_selected = table.column(0).checkboxes.selected();

                // Iterate over all selected checkboxes
                $.each(rows_selected, function(index, rowId){
                    // Create a hidden element
                    $(form).append(
                        $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', 'clientId[]')
                            .val(rowId)
                    );
                });

            });
        });

        
        // Gestion du select All :
        // $('[name="select_all"]').on('click', function(){
        //     $('input:checkbox').not(this).prop('checked', this.checked);
        // });

        // Gestion des boutons de recherches Shows et Langues :
        // $('.searchShowLang').on('click', function(){
        //     var table = $('#example').DataTable();
        //     var dataInfo = $(this).data('name');
        //     nombreRecherche = $('.tr-click').find( $('[data-info="' + dataInfo + '"]') );
        //     if(dataInfo != 'All'){
        //         table.search( dataInfo ).draw();
        //     } else {
        //         table.search('').draw();
        //     }
        // })

        // Gestion du Loader :
        setTimeout(function(){
            $('.wrapper').css({ 'opacity':'0' });
            $('.listeClientsPage').css({ 'display':'inherit' });
        }, 3000);

    </script>

@endsection