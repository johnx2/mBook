<!DOCTYPE html />
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!-- Icon -->
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>{{ config('app.name', 'mBook') }} - Mineralphil Madagascar</title>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
        
        <!--Import Google Icon Font-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
        <!-- Styles -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

        <!-- Bootstrap -->
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" />

        @yield('css')

    </head>
    <body>
        <div id="app">
            @if (!Auth::guest())
            <div class="navbar-fixed">
                <!-- Authentication Links -->
                <!-- @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                @else
                @endif -->
                <!-- Dropdown Structure -->
                <!-- <div class="dropdown">
                    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown trigger
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        ...
                    </ul>
                </div> -->
                
                <nav>
                    <div class="nav-wrapper">
                        <a href="{{ url('/') }}" class="brand-logo">{{ config('app.name', 'Laravel') }}</a>
                        <ul class="right hide-on-med-and-down menu-on-top">
                            <!-- Dropdown Trigger -->
                            <li class="dropdown">
                                <a id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-css" aria-labelledby="dLabel">
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Déconnexion
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                                
                                <!-- <a id="dLabel" class="dropdown-trigger" href="#!" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a>
                                <ul class="dropdown-content dropdown-menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Déconnexion
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul> -->
                            </li>
                        </ul>
                        <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                    </div>
                </nav>
                <ul id="slide-out" class="sidenav">
                    <li>
                        <div class="user-view">
                            <div class="background">
                                <img src="{{ asset('img/img-background.jpg') }}">
                            </div>
                            <a href="#!user"><img class="circle" src="{{ asset('img/user.png') }}"></a>
                            <a href="#!name"><span class="white-text name">{{ Auth::user()->name }}</span></a>
                            <a href="#!email"><span class="white-text email">{{ Auth::user()->email }}</span></a>
                        </div>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Déconnexion
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                    <!-- <li><div class="divider"></div></li> -->
                </ul>
                
            </div>
            @endif

            @yield('content')
        </div>

        <!-- Scripts -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <!-- <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>  -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- <script src="{{ asset('js/app.js') }}"></script> -->
        <script>
            $(document).ready(function(){

                $('.dropdown-toggle').dropdown();

                $('.sidenav').sidenav({
                    menuWidth: 300,
                    edge:'right',
                });
                $('.drag-target').css({'width':'60px'});

                /**
                 * Gestion affichage success / error :
                 */
                setTimeout(function(){
                    $('.alert').fadeOut(1000);
                }, 7000);

            });
        </script>

        @yield('js')
        
    </body>
</html>
