@extends('layouts.app')

@section('content')
<div class="container homePage">
    <div class="row">
        <!-- Message error-success -->
        @if (session('status'))
            <div class="col-md-12 alert alert-success">
                {{ session('status') }}
            </div>
        @endif
    </div>
    <div class="row">            

        <div class="col-sm-12 col-md-8 col-md-offset-2">

            <div class="row">
                <!-- {# Ajout client #} -->
                <article role="article" class="col-xs-6 col-sm-5">
                    <a href="{{ url('ajout-clients') }}">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                        <span>Ajout client</span>
                    </a>
                </article>

                <!-- {# Envoi mail #} -->
                <article role="article" class="col-xs-6 col-sm-5 col-sm-offset-2">
                    <a href="{{ url('gestion-mails') }}">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <span>Envoie mail</span>
                    </a>
                </article>
                
                <!-- {# Liste clients #} -->
                <article role="article" class="col-xs-6 col-sm-5">
                    <a href="{{ url('liste-clients') }}">
                        <i class="fa fa-id-card-o" aria-hidden="true"></i>
                        <span>Liste client(s)</span>
                    </a>
                </article>
                
                <!-- {# Stats #} -->
                <article role="article" class="col-xs-6 col-sm-5 col-sm-offset-2">
                    <a href="{{ url('stats') }}">
                        <i class="fa fa-bar-chart" aria-hidden="true"></i>
                        <span>Stats</span>
                    </a>
                </article>
            </div>

        </div>

    </div>
</div>
@endsection

@section('js')

    <script>

        // M.toast({html: '<i class="material-icons">plus_one</i> Client ajouté!'});

        /**
        * Animations d'introduction HOME
        */
            var blockHome1 = $( ".homePage article" ).eq(0);
            var blockHome2 = $( ".homePage article" ).eq(1);
            var blockHome3 = $( ".homePage article" ).eq(2);
            var blockHome4 = $( ".homePage article" ).eq(3);
            var vitesseHome = 500;

            blockHome1.animate({
                opacity: 1
            }, vitesseHome, function() {
                
                blockHome2.animate({
                    opacity: 1
                }, vitesseHome, function() {
                    
                    blockHome3.animate({
                        opacity: 1
                    }, vitesseHome, function() {
                        
                        blockHome4.animate({
                            opacity: 1
                        }, vitesseHome, function() {
                            
                        });
                    });
                });
            });
    </script>

@endsection