<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <meta charset="utf-8">
    </head>

    <style>
        .police1
        {
            font-size:12px;
            font-family: Andale Mono, monospace;
            text-align: center;
        }
    </style>

    <body>

        <div style="text-align:center">
            <h2 style="color:#21b7ba">Vous avez demandé une réinitialisation de mot de passe</h2>
        </div>

        <div>
            <p>Voici les informations pour vous connecter à votre espace.</p>
            <ul>
              <li>E-mail de connection : {{$user['email']}}</li>
              <li>Vous pouvez reinitialiser votre mot de passe ici : {{$url}}</li>
              <li>Si vous n'avez pas fait cette demande, merci d'ignorer cet e-mail.</li>
            </ul>

        </div><br /><br />

        <p class="police1">Ceci est un e-mail automatique, veuillez ne pas répondre directement à ce message.</p>

        <br/><br/><br/><br/>

    </body>
</html>