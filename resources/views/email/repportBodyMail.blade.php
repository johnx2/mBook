<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <meta charset="utf-8">
    </head>

    <style>
        .police1
        {
            font-size:12px;
            font-family: Andale Mono, monospace;
            text-align: center;
        }
    </style>

    <body>

        <p>Process numéro : <?php echo $process_name; ?></p>
        <p>Le script s'est terminer avec <?php echo $totalSuccess; ?> mail(s) envoyé(s), <?php echo $totalClient - $totalSuccess; ?> mail(s) échoué(s) sur <?php echo $totalClient; ?> mail(s) total.</p>

    </body>
</html>