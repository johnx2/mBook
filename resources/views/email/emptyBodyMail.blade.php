<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <meta charset="utf-8">
    </head>

    <style>
        .police1
        {
            font-size:12px;
            font-family: Andale Mono, monospace;
            text-align: center;
        }
    </style>

    <body>

        <?php echo $content; ?>

    </body>
</html>