@extends('layouts.app')

@section('css')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <style>
        .modal {
            background-color: transparent!important;
            box-shadow: none!important;
            width: auto!important;
        }
    </style>
@endsection

@section('content')
<div class="container gestionMailsPage">
    <div class="row">
        <!-- Message error-success -->
        @if (session('status'))
            <div class="col-md-12 alert alert-success">
                {{ session('status') }}
            </div>
        @endif
    </div>
    <div class="row">      

        <div class="col-sm-12 col-md-12">
            <div class="card left-align transparent">
                <div class="header">
                    <h2>
                        Gestion envoie Mails
                    </h2>
                </div>
            </div>
        </div>      

        <div class="col-sm-12 col-md-6">

            <div class="bloc-entete row">
                <div class="col-sm-12 col-md-6 collapsible-header-total-client">
                    <i class="material-icons">filter_drama</i>Total client(s) : {{ $totalClientMail }}
                </div>
                <div class="col-sm-12 col-md-6 collapsible-header-autocomplete input-field">
                    <input type="text" id="autocomplete-input" class="autocomplete">
                    <label for="autocomplete-input">Autocomplete</label>
                </div>
            </div>
            <div class="listeMails">
                <table class="table">
                @if($clientsMail != [])
                    @foreach($clientsMail as $client)
                    <tr data-id="{{ $client->id }}">
                        <td>{{ $client->id }}</td>
                        <td><span data-mail="{{ $client->email }}" class="mailName"><< {{ $client->email }} >></span> @if($client->nom != null){{ $client->nom }}@endif @if($client->prenom != null){{ $client->prenom }}@endif</td>
                        <td><i class="fa fa-building-o prefix"></i> @if($client->societe != null){{ $client->societe }}@else - @endif</td>
                        <td><i class="fa fa-minus-circle" aria-hidden="true"></i></td>
                    </tr>
                    @endforeach
                @else 
                    <tr>
                        <td>Aucun id</td>
                    </tr>
                @endif
                </table>
            </div>

        </div>
        <div class="col-sm-12 col-md-6">

            <!-- choix du SMTP -->
            <div class="input-field">
                <select class="form-control" required name="smtp">
                    <!-- <option value="" disabled selected>Préférence show</option> -->
                    @foreach($smtpClientMail as $smtp)
                        <!-- gestion uniquement du smtp ovh -->
                        @if($smtp['nom_smtp'] != 'gmail')
                            <option class="valueSmtp" value="{{ $smtp['adresse_smtp'] }}">{{ $smtp['nom_smtp'] }} [{{ $smtp['label'] }}] ({{ $smtp['total_heure_max'] }} h total)</option>
                        @else
                            <option disabled class="disabled valueSmtp" value="{{ $smtp['adresse_smtp'] }}">{{ $smtp['nom_smtp'] }} [{{ $smtp['label'] }}] ({{ $smtp['total_heure_max'] }} h total [non disponible])</option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="input-field subjectBloc">
                <i class="fa fa-road prefix"></i>
                <input id="subject" name="subject" type="text" class="validate" required="required" />
                <label for="subject">Sujet du mail</label>
            </div>

            <form method="post" autocomplete="off">
                <textarea id="summernote" name="editordata" required></textarea>
            </form>

        </div>
        <div class="col-sm-12 col-md-4 col-md-offset-4">

            @if($clientsMail != [])
                <button class="btn btn-warning waves-effect waves-light mailVerification" type="submit" name="mailVerification">
                    Vérifier le mail
                    <i class="material-icons right">send</i>
                </button>
            @else 
                <button class="btn btn-warning mailVerificationDisabled" type="button">
                    Vérifier le mail
                    <i class="material-icons right">send</i>
                </button>
            @endif

            <!-- Modal -->
            <div class="modal fade" id="spamRepport" tabindex="-1" role="dialog" aria-labelledby="spamRepportLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content card left-align transparent">
                        <div class="modal-header header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="spamRepportLabel">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            ...
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <button type="button" class="btn btn-primary" id="envoieMailFinal">Envoyer le mail</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
@endsection

@section('js')

    <!-- Ajout de la scrollbar-y en auto -->
    <script>
        $('body').css({ 'overflow-y': 'scroll' });
    </script>

    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script src="{{ asset('js/summernoteFR.js') }}"></script>

    <script>
        
        $(document).ready(function(){
            $('.collapsible').collapsible();

            // Search bar :
            $('input.autocomplete').autocomplete({
                data: {
                    "Apple": null,
                    "Microsoft": null,
                    "Google": 'https://placehold.it/250x250'
                },
            });

            // WYSIWYG editor :
            $('#summernote').summernote({
                placeholder: 'Votre message ..',
                height: 150,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: true,                  // set focus to editable area after initializing summernote
                lang: 'fr-FR', // default: 'en-US'
                // dialogsInBody: true,
                toolbar: [
                    ['style', ['style']],
                    ['style', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video', 'hr', 'readmore']],
                    ['genixcms', ['elfinder']],
                    ['view', ['fullscreen', 'codeview']],
                    ['help', ['help']]
                ]
            });

            // Gestion bouton ENVOIE MAIL :
            $('[name="mailVerification"]').on('click', function(e){
                e.preventDefault();

                var listeTr = $('.listeMails tr');
                var arrayId = [];
                listeTr.each(function() {
                    arrayId.push($( this ).data('id'));
                });
                var smtp = $('[name="smtp"]').val();
                var sujet = $('[name="subject"]').val();
                var message = $('#summernote').val();

                // On lance l'ajax passion Eucaliptus
                $.ajax({

                    type: 'POST',
                    url: '{{ url("/gestion-mails/checkMail") }}',
                    data: { _token: "{{ csrf_token() }}", message: message, sujet: sujet, smtp: smtp, arrayId: arrayId },
                    // dataType: 'JSON',
                    error: function(result, status, error) { 
                        console.log("Réponse jQuery : " + result);
                        console.log("Statut de la requète : " + status);
                        console.log("Type d’erreur : " + error); 
                    },

                    // Si mon bon ajax me ramene de joli chose
                    success: function(data){
                        // console.table(data);
                        if(data.error === 42){

                            $('#spamRepport .modal-title').html("Rapport Anti-Spam <span>(" + data.totalFrom + " client(s) - Score : <strong>N.A.</strong>)</span>");
                            $('#spamRepport .modal-body').html("<div>Aucun message et/ou sujet.</div>");
                            $('#spamRepport #envoieMailFinal').addClass('disabled');
                            $('#spamRepport').modal('show');

                        } else {

                            $('#spamRepport .modal-title').html("Rapport Anti-Spam <span>(" + data.totalFrom + " client(s) - Score : <strong>" + data.spamScore + "</strong>)</span>");
                            var html = "<table class='table'>";
                            html += "<tr><th>Règle</th><th>Description</th><th>Complément</th><th>Score</th></tr>";
                            for(var i = 0; i < data.spamReport.length; i++){
                                // console.log(data.spamReport[i]);
                                html += "<tr>";
                                html += "<td>" + data.spamReport[i].name + "</td><td>" + data.spamReport[i].info + "</td><td>" + data.spamReport[i].complement + "</td><td>" + data.spamReport[i].score + "</td>";
                                html += "</tr>";
                            }
                            html += "</table>";
                            $('#spamRepport .modal-body').html("<div>" + html + "</div>");
                            if(data.spamScore >= 5){
                                $('#spamRepport #envoieMailFinal').addClass('disabled');
                            } else {
                                $('#spamRepport #envoieMailFinal').removeClass('disabled');
                            }
                            $('#spamRepport').modal('show');

                            $('#spamRepport #envoieMailFinal').on('click', function(z){
                                $(this).attr('disabled', 'disabled');
                                
                                if($('#spamRepport #envoieMailFinal').hasClass('disabled') == false){
                                    
                                    // $('#spamRepport').modal('hide');

                                    // On lance l'ajax passion Eucaliptus
                                    $.ajax({

                                        type: 'POST',
                                        url: '{{ url("/gestion-mails/store") }}',
                                        data: { _token: "{{ csrf_token() }}", content: data },
                                        // dataType: 'JSON',
                                        error: function(result, status, error) { 
                                            console.log("Réponse jQuery : " + result);
                                            console.log("Statut de la requète : " + status);
                                            console.log("Type d’erreur : " + error); 
                                        },
                                        success: function(retour) {

                                            if(retour != null){
                                                // Sécurité anti retour arriere :
                                                $('[name="smtp"]').val('');
                                                $('[name="subject"]').val('');
                                                $('#summernote').val('');
                                                $('.listeMails table').html('<tr><td>Aucun id</td></tr>');
                                                // Redirection :
                                                var baseUrl = '{{ url("/stats/mail-detail") }}';
                                                var urlFinal = baseUrl + '/' + retour;
                                                window.location.href = urlFinal;
                                            } else {
                                                window.location.href = '{{ url("gestion-mails") }}';
                                            }
                                        }

                                    });

                                }
                            });

                        }
                    }

                });

            });

        });

    </script>

@endsection