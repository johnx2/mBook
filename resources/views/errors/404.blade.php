<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!-- Icon -->
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Quicksand:400,700&amp;subset=latin-ext" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <title>{{ config('app.name', 'mBook') }} - Mineralphil Madagascar</title>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
        
        <!--Import Google Icon Font-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
        <!-- Styles -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

        <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" />
    </head>

    <body class="four-one-nine">
    <div class="container-fluid four-one-nine-container">
        <div class="row m-b-50">
            <div class="error-code col-xs-8 col-xs-offset-2 col-md-4 col-md-offset-4">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('img/logo.png') }}" class="img-responsive" width="100%" alt="Mineralphil madagascar"/>
                </a>
            </div>
        </div>
        <div class="error-message">
            
            <h2><strong>Awww... Pas de panique.</strong></h2>
            C'est juste une erreur 404 !<br />
            La page que tu recherches n'existe pas ;)

        </div>
    </div>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!-- Bootstrap js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

    </body>

</html>