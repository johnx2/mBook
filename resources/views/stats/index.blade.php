@extends('layouts.app')

@section('css')
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" /> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
@endsection

@section('content')
<div class="wrapper">
    Chargement ..
    <div class="loader">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<div class="container statsPage">

    <!-- AFFICHAGE DES MESSAGES D ERREURS -->
    <div class="row m-t-10">
        <!-- Message error-success -->
        @if (session('status'))
            <div class="col-md-12 alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @if (session('error'))
            <div class="col-md-12 alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
    </div>

    <!-- ARRAY -->
    <div class="row">            
        <div class="col-sm-12 col-md-12">
            <div class="card left-align transparent">
                <div class="header">
                    <h2>
                        Statistiques
                    </h2>
                </div>
            </div>
        </div> 

        <div class="col-sm-12 col-md-12">
                
            {{-- {{ dump($mailHistory) }} --}}
            
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr style="display:none">
                        <th>-</th>
                        <th>-</th>
                        <th>-</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($arrayHistoryMail as $ligne)
                        <tr class="tr-click">
                            <td>
                                <div class="row">
                                    {{-- bloc rond gauche --}}
                                    <div class="mail-ico col-xs-3 col-xs-offset-1 col-sm-1">
                                        <div class="mail-icon">{{ ucfirst($ligne['mail_from_label'][0]) }}</div>
                                    </div>
                                    {{-- bloc text droite --}}
                                    <div class="mail-text col-xs-8 col-sm-10">
                                        <a href="{{ url('stats/mail-detail', ['process_name' => $ligne['process_name']]) }}">
                                            {{-- FROM --}}
                                            <div>
                                                {{ $ligne['mail_from_label'] }}
                                            </div>
                                            {{-- Subject --}}
                                            <div>
                                                {{ $ligne['mail_subject'] }}
                                            </div>
                                            {{-- Body --}}
                                            <div>
                                                {{ str_limit(strip_tags($ligne['mail_body']), $limit = 50, $end = '...') }}
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <td class="fa-edit-td">
                                {{-- Date --}}
                                <div class="mail-date" title="{{ $ligne['created_at'] }}">
                                    {{ $ligne['created_at_humain'] }}.
                                </div>
                                {{-- Nbr clients --}}
                                <div class="mail-nbr-client">
                                    @if($ligne['total_client'] > 1)
                                        {{ $ligne['total_client'] }} clients <i class="fa fa-users" aria-hidden="true"></i>
                                    @else
                                        {{ $ligne['total_client'] }} client <i class="fa fa-user" aria-hidden="true"></i>
                                    @endif
                                </div>
                            </td>
                            <td class="fa-edit-td hidden-xs">
                                {{-- Voir le détail --}}
                                <a href="{{ url('stats/mail-detail', ['process_name' => $ligne['process_name']]) }}">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    
                </tbody>
                <tfoot>
                    <tr style="display:none">
                        <th>-</th>
                        <th>-</th>
                        <th>-</th>
                    </tr>
                </tfoot>

            </table>

        </div>

    </div>
    
</div>
@endsection

@section('js')

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script>

        // Ajout d'une scrollbar native :
        // $('html').css({ 'overflow-y': 'scroll' });

        // Si la fenetre est plus petite que 767px on n affiche que 3 buttons pagination :
        var largeurWindow = $(window).width();
        if (largeurWindow <= 767){
            $.fn.DataTable.ext.pager.numbers_length = 3;
        }
        // Gestion du DataTable :
        $('#example').DataTable({
            // "scrollY":        "340px",
            "scrollCollapse": true,
            "paging":         true,
            "ordering": false,
            "info":     true,
            "language": {
                processing:     "Traitement en cours...",
                search:         "Rechercher&nbsp;:",
                lengthMenu:    "Afficher _MENU_ ligne(s)",
                info:           "Affichage de ligne _START_ &agrave; _END_ sur _TOTAL_ ligne(s)",
                infoEmpty:      "Affichage de ligne 0 &agrave; 0 sur 0 ligne(s)",
                infoFiltered:   "(filtr&eacute; de _MAX_ ligne(s) au total)",
                infoPostFix:    "",
                loadingRecords: "Chargement en cours...",
                zeroRecords:    "Aucun ligne &agrave; afficher",
                emptyTable:     "Vous n'avez envoy&eacute; aucun mail",
                paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                },
                aria: {
                    sortAscending:  ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            }
        });

        // Gestion du Loader :
        setTimeout(function(){
            $('.wrapper').css({ 'opacity':'0' });
            $('.statsPage').css({ 'opacity':'1' });
        }, 3000);

    </script>

@endsection