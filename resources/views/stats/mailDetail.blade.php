@extends('layouts.app')

@section('css')

@endsection

@section('content')
    <div class="container statsMailDetail">

        <!-- AFFICHAGE DES MESSAGES D ERREURS -->
        <div class="row m-t-10">
            <!-- Message error-success -->
            @if (session('status'))
                <div class="col-md-12 alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="col-md-12 alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
        </div>

        <!-- ARRAY -->
        <div class="row">            
            <div class="col-sm-12 col-md-12">
                <div class="card left-align transparent">
                    <div class="header">
                        <h2 class="navigateBeforeHeader">
                            <a href="{{ url('stats') }}">
                                <i class="material-icons">navigate_before</i>
                            </a>
                            <span>
                                Détail de l'envoi du {{ $arrayHistoryMail['created_at'] }} par <a href="mailto:{{ $arrayHistoryMail['user_mail'] }}">{{ $arrayHistoryMail['user_name'] }}</a>
                            </span>
                        </h2>
                    </div>
                </div>
            </div> 

            <div class="col-sm-12 col-md-6">
                
                <div class="mail-show-progress">
                    <div class="mail-show-progress-header">
                        Total envoie : xx/xx
                        <div class="progress">
                            <div class="determinate" style="width: 100%"></div>
                        </div>
                    </div>
                    <table id="example" class="table" style="width:100%">
                        
                        <thead>
                            <tr style="display:none">
                                <th>-</th>
                                <th>-</th>
                            </tr>
                        </thead>
                        <tbody class="frameSup">
                            {{-- Chargement des envoies en live ;) --}}
                        </tbody>

                    </table>
                </div>

                @php
                    if( $arrayHistoryMail['spam_score'] >= 5 ){
                        $colorCss = 'send-not-ok';
                    } elseif( $arrayHistoryMail['spam_score'] >= 3 ){
                        $colorCss = 'send-in-progress';
                    } else {
                        $colorCss = 'send-ok';
                    }
                @endphp
                <div class="bloc-detail-report {{ $colorCss }}">
                    <i class="fa fa-trophy" aria-hidden="true"></i>
                    <div class="spam-score">{{ $arrayHistoryMail['spam_score'] }}</div>
                    <span></span>
                    <p>SPAM score</p>
                </div>

            </div>

            <div class="col-sm-12 col-md-6">

                <div class="card blue-grey darken-1 bloc-show-message">
                    <div class="card-content white-text">
                        <span class="card-title"><strong>Prévisualisation du message</strong></span>
                    </div>
                    <div class="card-action left-align bloc-info-message">
                        De : <strong>{{ $arrayHistoryMail['mail_from_label'] }}</strong> < {{ $arrayHistoryMail['mail_from_adresse_smtp'] }} >
                    </div>
                    <div class="card-action left-align bloc-info-message">
                        A : @if($arrayHistoryMail['total_client'] > 1) {{ $arrayHistoryMail['total_client'] }} clients @else {{ $arrayHistoryMail['total_client'] }} client @endif
                    </div>
                    <div class="card-action left-align bloc-info-message">
                        Sujet : {{ $arrayHistoryMail['mail_subject'] }}
                    </div>
                    <div class="card-action bloc-show-mail-message">
                        <iframe sandbox seamless srcdoc='{{ $arrayHistoryMail['mail_body'] }}'>
                             Du contenu pour les navigateurs qui ne supportent pas
                             les iframes.
                        </iframe>
                    </div>
                </div>

            </div>

        </div>
        
    </div>
@endsection

@section('js')

    <script>

        // Ajout d'une scrollbar native :
        // $('html').css({ 'overflow-y': 'scroll' });

        var processName = "{{ $arrayHistoryMail['process_name'] }}";
        
        function processRefresh(processName){

            var baseUrl = '{{ url("/stats/get-stats-mail") }}';
            var urlFinal = baseUrl + '/' + processName;
            $.ajax({
				type: "GET",
				url: urlFinal,
				error: function (result, status, error) {
					console.log("Réponse jQuery : " + result);
					console.log("Statut de la requète : " + status);
					console.log("Type d’erreur : " + error);

                    $('.statsMailDetail table .frameSup').html('<div class="vertical-align"><i class="material-icons m-r-10">graphic_eq</i>Une erreur est survenue.</div>');
                    $('.mail-show-progress-header').text('Total envoie : xx/xx');
                },
				success: function (data) {
                    
                    $('.frameSup').html('');

                    if(data){
                        var html = '';
                        var totalClients = data.length;
                        var totalOk = 0;
                        // var total
                        for(var i = 0; i < data.length; i++){
                            html += '<tr>';
                            html += '<td>' + data[i].job + '</td><td>';
                            if(data[i].status == 0){ 
                                html += '<span class="send-in-progress"><i class="fa fa-refresh fa-spin" aria-hidden="true"></i></span>'; 
                            } else {
                                totalOk++;
                                html += '<span class="send-ok"><i class="fa fa-check" aria-hidden="true"></i></span>';
                            }
                            html += '</td></tr>';
                        }
                        $('.frameSup').html(html);
                        var pourcentBar = (100 * totalOk) / totalClients;
                        $('.mail-show-progress-header').html('Total envoie : ' + totalOk + '/' + totalClients + '<div class="progress"><div class="determinate" style="width: ' + pourcentBar + '%"></div></div>');
                    }

                }
            });

        }

        processRefresh(processName);
        setInterval("processRefresh(processName)", 3000);

    </script>

@endsection