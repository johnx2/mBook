@extends('layouts.app')

@section('content')
<div class="container loginPage">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <a href="{{ url('/') }}">      
                <img src="{{ asset('img/logo.png') }}" alt="Logo du site" class="img-responsive" />
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Connexion</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <!-- <label for="email" class="col-md-4 control-label">Adresse E-mail</label> -->

                            <div class="input-field col-md-10 col-md-offset-1">

                                <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required>
                                <label for="email">Adresse E-mail</label>
                                
                                <!-- <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus> -->

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <!-- <label for="password" class="col-md-4 control-label">Mot de passe</label>    -->

                            <div class="input-field col-md-10 col-md-offset-1">

                                <input id="password" type="password" class="validate" name="password" required>
                                <label for="password">Password</label>

                                <!-- <input id="password" type="password" class="form-control" name="password" required> -->

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="col-md-10 col-md-offset-1 left-align">
                            <input type="checkbox" class="filled-in" id="filled-in-box"  name="remember" {{ old('remember') ? 'checked' : '' }}/>
                            <label for="filled-in-box">Se souvenir de moi</label>
                        </div>
                        
                        <div class="col-md-8 col-md-offset-4">
                            <button class="btn btn-warning waves-effect waves-light" type="submit" name="action">Connexion
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <a class="waves-effect waves-light" href="{{ route('password.request') }}">
            <i class="material-icons right">cloud</i>Mot de passe oublié ?
        </a>
    </div>
</div>
@endsection
