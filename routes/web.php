<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

// Auth() :
    // Auth::routes();
    // Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    // Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    // Route::post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('psynobulle-reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('psynobulle-reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('psynobulle-reset', 'Auth\ResetPasswordController@reset');

Route::middleware(['auth'])->group(function () {

    // Accueil :
    Route::get('/home', 'HomeController@index')->name('home');

    // Ajout Clients :
    Route::get('/ajout-clients', 'ClientController@ajout')->name('ajout-clients');
    Route::post('/ajout-clients', 'ClientController@store')->name('ajout-clients.store');
    Route::post('/ajout-clients/checkemail', 'ClientController@checkEmail')->name('ajout-clients.checkEmail');

    // Liste clients :
    Route::get('/liste-clients', 'ClientController@index')->name('liste-clients');
    Route::get('/downoloadListe', 'ClientController@downoloadListe')->name('downoloadListe');

    // Edition client :
    Route::get('/info-client/{id}', 'ClientController@show')->name('ajout-clients.show');
    Route::post('/info-client/{id}', 'ClientController@update')->name('ajout-clients.update');

    // Envoie e-mail :
    Route::get('/gestion-mails', 'MailController@index')->name('gestion-mails.index');
    Route::post('/gestion-mails', 'MailController@index')->name('gestion-mails.indexPost');
    Route::post('/gestion-mails/checkMail', 'MailController@checkMail')->name('gestion-mails.checkMail');
    Route::post('/gestion-mails/store', 'MailController@store')->name('gestion-mails.store');

    // Stats :
    Route::get('/stats', 'StatsController@index')->name('stats.index');
    Route::get('/stats/mail-detail/{processName}', 'StatsController@show')->name('stats.show');
    Route::get('/stats/get-stats-mail/{processName}', 'StatsController@getStatsMail')->name('stats.getStatsMail');

    Route::get('/parametres', 'ParamController@index')->name('params.index');
    Route::get('/parametres/datas/{type}', 'ParamController@showDatas')->name('params.showDatas');

});